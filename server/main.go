package main

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/pwed/stake-cli/stake"
)

var sc *stake.Client

func main() {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		// AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// 	return origin == "https://github.com"
		// },
		MaxAge: 12 * time.Hour,
	}))

	sc = stake.NewClient("../credentials.json")

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"endpoints": []string{
				"/marketStatus",
				"/graphData/equityPie",
			},
		})
	})
	r.GET("/marketStatus", marketStatusHandler)
	r.GET("/graphData/equityPie", graphDataEquityPieHandler)
	r.GET("/user", userHandler)
	r.GET("/funds", fundsHandler)

	r.Run(":8080")
}

func marketStatusHandler(c *gin.Context) {
	c.JSON(200, sc.GetMarketStatus())
}

func graphDataEquityPieHandler(c *gin.Context) {
	c.JSON(200, sc.GenerateEquityPie())
}

func userHandler(c *gin.Context) {
	c.JSON(200, sc.GetUser())
}

func fundsHandler(c *gin.Context) {
	c.String(200, string(sc.GetFundingHistory()))
}
