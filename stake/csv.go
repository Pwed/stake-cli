package stake

import (
	"fmt"
	"time"
)

func (c Client) CSVEquityValue() string {
	evUSD := c.GetPositions(true).EquityValue
	evAUD := c.GetValueInAUD()

	dt := time.Now().Format("2006-01-02 15:04:05")
	return fmt.Sprintf("%v,%.2f,%.2f", dt, evUSD, evAUD)
}
