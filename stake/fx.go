package stake

import "encoding/json"

type FXRate struct {
	BaseCurrency   string  `json:"baseCurrency"`
	TargetCurrency string  `json:"targetCurrency"`
	Period         string  `json:"period"`
	Value          float64 `json:"value"`
	Unit           string  `json:"unit"`
}

func (c Client) GetFXRate() *FXRate {
	var rate FXRate
	json.Unmarshal(c.get("/utils/getLatestFxRates"), &rate)
	return &rate
}

func (c Client) GetValueInAUD() float64 {
	valueUSD := c.GetPositions(true).EquityValue
	rateAUDToUSD := c.GetFXRate().Value
	return valueUSD * (1 / rateAUDToUSD)
}
