package stake

import (
	"encoding/json"
)

type UserProfile struct {
	ResidentialAddress string `json:"residentialAddress"`
	PostalAddress      string `json:"postalAddress"`
}

type User struct {
	UserID                     string      `json:"userId"`
	Username                   string      `json:"username"`
	EmailAddress               string      `json:"emailAddress"`
	DWID                       string      `json:"dw_id"`
	DWAccountID                string      `json:"dw_AccountId"`
	DWAccountNumber            string      `json:"dw_AccountNumber"`
	MacAccountNumber           string      `json:"macAccountNumber"`
	Status                     string      `json:"status"`
	MacStatus                  string      `json:"macStatus"`
	DWStatus                   string      `json:"dwStatus"`
	TruliooStatus              string      `json:"truliooStatus"`
	TruliooStatusWithWatchlist string      `json:"truliooStatusWithWatchlist"`
	FirstName                  string      `json:"firstName"`
	LastName                   string      `json:"lastName"`
	PhoneNumber                string      `json:"phoneNumber"`
	SignUpPhase                int         `json:"signUpPhase"`
	AckSignedWhen              string      `json:"ackSignedWhen"`
	CreatedDate                int         `json:"createdDate"`
	StakeApprovedDate          string      `json:"stakeApprovedDate"`
	AccountType                string      `json:"accountType"`
	MasterAccountID            string      `json:"masterAccountId"`
	ReferralCode               string      `json:"referralCode"`
	ReferredByCode             string      `json:"referredByCode"`
	RegionIdentifier           string      `json:"regionIdentifier"`
	AssetSummary               string      `json:"assetSummary"`
	FundingStatistics          string      `json:"fundingStatistics"`
	TradingStatistics          string      `json:"tradingStatistics"`
	W8File                     []string    `json:"w8File"`
	EmailVerified              bool        `json:"emailVerified"`
	HasFunded                  bool        `json:"hasFunded"`
	HasTraded                  bool        `json:"hasTraded"`
	RewardJourneyTimestamp     int         `json:"rewardJourneyTimestamp"`
	RewardJourneyStatus        string      `json:"rewardJourneyStatus"`
	UserProfile                UserProfile `json:"userProfile"`
	LedgerBalance              float64     `json:"ledgerBalance"`
	InvestorAccreditations     string      `json:"investorAccreditations"`
	CanTradeOnUnsettledFunds   bool        `json:"canTradeOnUnsettledFunds"`
	Mfaenabled                 bool        `json:"mfaenabled"`
}

func (c Client) GetUser() *User {
	var u User
	json.Unmarshal(
		c.requestWithBody(
			"GET",
			"/user",
			[]byte(`{"responseFilters":["REWARD_JOURNEY"]}`),
		),
		&u,
	)
	return &u
}
