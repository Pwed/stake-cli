package stake

import "time"

type timeRange struct {
	StartDate time.Time
	EndDate   time.Time
}

type Event struct {
	ID              string
	Timestamp       string
	OrderType       string
	EventType       string
	Status          string
	Title           string
	Amount          string
	Description     string
	CurrencyFrom    string
	CurrencyTo      string
	Symbol          string
	StockImageURL   string
	Side            string
	NumberOfShares  float64
	AmountOnly      float64
	AmountLimit     int
	AmountStop      int
	EffectivePrice  float64
	SpotRate        float64
	TotalFee        float64
	AmountFrom      float64
	AmountTo        float64
	Rate            float64
	ReferenceNumber string
}

type History []Event

func (c Client) GetAllHistory() []byte {
	// tr := timeRange{
	// 	StartDate: time.Unix(int64(u.CreatedDate), 0),
	// 	EndDate:   time.Now(),
	// }
	return c.requestWithBody(
		"post",
		"/utils/activityLog",
		[]byte(`{"endDate":"05/10/2020","startDate":"05/04/2020"}`),
	)
}

func (c Client) GetFundingHistory() []byte {
	// tr := timeRange{
	// 	StartDate: time.Unix(int64(u.CreatedDate), 0),
	// 	EndDate:   time.Now(),
	// }
	return c.requestWithBody(
		"post",
		"/utils/activityLog/fundingOnly",
		[]byte(`{"endDate":"05/10/2020","startDate":"05/04/2020"}`),
	)
}
