package stake

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// type credentials struct {
// 	Username string `json:"username"`
// 	Password string `json:"password"`
// }

type session struct {
	SessionKey string `json:"sessionKey"`
}

type Client struct {
	session session
}

const apiURL = "https://global-prd-api.hellostake.com/api"

// func newCreds(path string) *credentials {
// 	jsonFile, err := os.Open(path)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	defer jsonFile.Close()
// 	byteValue, _ := ioutil.ReadAll(jsonFile)
// 	var creds credentials
// 	json.Unmarshal(byteValue, &creds)

// 	return &creds
// }

func readSession(path string) *session {
	jsonFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var sess session
	json.Unmarshal(byteValue, &sess)

	return &sess
}

func NewClient(path string) *Client {
	sess := readSession(path)
	// byteValue, _ := json.Marshal(creds)
	// resp, err := http.Post(apiURL+"/sessions/createSession", "application/json", bytes.NewBuffer(byteValue))
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// bodyBytes, _ := ioutil.ReadAll(resp.Body)
	// var sess session
	// json.Unmarshal(bodyBytes, &sess)
	// fmt.Println(sess.SessionKey)
	return &Client{session: *sess}
}

func (c Client) get(path string) []byte {

	client := &http.Client{}
	request, err := http.NewRequest("GET", apiURL+path, bytes.NewBuffer([]byte{}))
	request.Header.Set("Stake-Session-Token", c.session.SessionKey)

	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}

	// if resp.StatusCode != http.StatusOK {

	// }

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	// log.Fatal(string(bodyBytes))
	return bodyBytes
}

func (c Client) requestWithBody(method, path string, body []byte) []byte {

	client := &http.Client{}
	request, err := http.NewRequest(method, apiURL+path, bytes.NewBuffer(body))
	request.Header.Set("Stake-Session-Token", c.session.SessionKey)
	request.Header.Set("Content-Type", "application/json")

	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	// if resp.StatusCode != http.StatusOK {
	log.Fatal(resp)
	// }

	bodyBytes, err := ioutil.ReadAll(resp.Body)

	return bodyBytes
}
