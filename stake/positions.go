package stake

import "encoding/json"

type EquityPosition struct {
	UnrealizedDayPLPercent string  `json:"unrealizedDayPLPercent"`
	Symbol                 string  `json:"symbol"`
	EncodedName            string  `json:"encodedName"`
	UnrealizedDayPL        string  `json:"unrealizedDayPL"`
	InstrumentID           string  `json:"instrumentID"`
	OpenQty                string  `json:"openQty"`
	PriorClose             string  `json:"priorClose"`
	AvgPrice               string  `json:"avgPrice"`
	Side                   string  `json:"side"`
	CostBasis              string  `json:"costBasis"`
	MktPrice               string  `json:"mktPrice"`
	MarketValue            string  `json:"marketValue"`
	UnrealizedPL           string  `json:"unrealizedPL"`
	AvailableForTradingQty string  `json:"availableForTradingQty"`
	Name                   string  `json:"name"`
	Category               string  `json:"category"`
	URLImage               string  `json:"urlImage"`
	BidPrice               string  `json:"bidPrice"`
	AskPrice               string  `json:"askPrice"`
	ReturnOnStock          string  `json:"returnOnStock"`
	LastTrade              float64 `json:"lastTrade"`
	DailyReturnValue       float64 `json:"dailyReturnValue"`
	YearlyReturnValue      float64 `json:"yearlyReturnValue"`
	YearlyReturnPercentage float64 `json:"yearlyReturnPercentage"`
	Period                 string  `json:"period"`
}

type EquityPositions struct {
	Positions   []EquityPosition `json:"equityPositions"`
	EquityValue float64          `json:"equityValue"`
	PricesOnly  bool             `json:"pricesOnly"`
}

func (c Client) GetPositions(pricesOnly bool) *EquityPositions {
	var positions EquityPositions
	path := "/users/accounts/v2/equityPositions"
	if pricesOnly {
		path = path + "/pricesOnly"
	}
	json.Unmarshal(c.get(path), &positions)
	return &positions
}
