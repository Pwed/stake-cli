package stake

//TODO remove need for njson by creating a custom unmarshal function

import (
	"github.com/m7shapan/njson"
)

type MarketStatus struct {
	Date           string `njson:"response.date"`
	ElapsedTime    string `njson:"response.elapsedtime"`
	Error          string `njson:"response.error"`
	Message        string `njson:"response.message"`
	StatusChangeAt string `njson:"response.status.change_at"`
	StatusCurrent  string `njson:"response.status.current"`
	StatusNext     string `njson:"response.status.next"`
	UnixTime       string `njson:"response.unixtime"`
	VersionNumber  string `njson:"response.versionNumber"`
}

func (c Client) GetMarketStatus() *MarketStatus {
	var status MarketStatus
	njson.Unmarshal(c.get("/utils/marketStatus"), &status)
	return &status
}
