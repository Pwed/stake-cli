package stake

import (
	"fmt"
	"sort"
	"strconv"
)

type Pie struct {
	Labels   []string  `json:"labels"`
	Datasets []Dataset `json:"datasets"`
}

type Dataset struct {
	Data                 []string `json:"data"`
	BackgroundColor      []string `json:"backgroundColor"`
	HoverBackgroundColor []string `json:"hoverBackgroundColor"`
}

func (c Client) GenerateEquityPie() Pie {
	p := c.GetPositions(false)
	pie := Pie{
		[]string{},
		[]Dataset{
			{
				[]string{},
				[]string{},
				[]string{},
			},
		},
	}

	colors := []string{
		"#F00",
		"#0F0",
		"#FF0",
		"#00F",
	}
	hoverColors := []string{
		"#F88",
		"#8F8",
		"#FF8",
		"#88F",
	}

	sort.SliceStable(p.Positions, func(i, j int) bool {

		iv, _ := strconv.ParseFloat(p.Positions[i].MarketValue, 64)
		jv, _ := strconv.ParseFloat(p.Positions[j].MarketValue, 64)

		return iv < jv

	})

	for i := len(p.Positions) - 1; i >= 0; i-- {
		// if !(c < len(colors)) {
		// 	c = 0
		// }
		position := p.Positions[i]

		ev := position.MarketValue
		label := fmt.Sprintf("%v", position.Symbol)

		pie.Labels = append(pie.Labels, label)
		pie.Datasets[0].Data = append(pie.Datasets[0].Data, ev)
		pie.Datasets[0].BackgroundColor = append(pie.Datasets[0].BackgroundColor, colors[i%len(colors)])
		pie.Datasets[0].HoverBackgroundColor = append(pie.Datasets[0].HoverBackgroundColor, hoverColors[i%len(colors)])
	}

	// j, _ := json.MarshalIndent(pie, "", "  ")
	return pie
}
