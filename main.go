package main

import (
	"fmt"

	"gitlab.com/pwed/stake-cli/stake"
)

func main() {
	sc := stake.NewClient("credentials.json")
	// fmt.Println(sc.CSVEquityValue())
	fmt.Println(
		sc.GetPositions(true),
	)
	// fmt.Println(sc.GetPositionsPricesOnly())
	// fmt.Printf("%+v\n", sc.GetMarketStatus())
}
