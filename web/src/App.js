import React from 'react';
import EquityPie from './components/pie'
import ValueLine from './components/line'
import './App.css';

function App() {
  return (
    <div className="App">
      {/* <ValueLine /> */}
      <EquityPie />
    </div>
  );
}

export default App;
