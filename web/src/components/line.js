import React from 'react';
import { Line } from 'react-chartjs-2';

function withChartSizeControl(Component) {
	return props => (
		<div
			className="chart"
			style={{
				position: "relative",
				height: props.height+"%",
                width: props.width+"%",
                float: "right",
                display: "block",
			}}
		>
			<Component {...props} />
		</div>
	);
}

const NewLine = withChartSizeControl(Line);

class ValueLine extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ValueOverTime: {
                datasets: [{
                    label: "USD",
                    borderColor: "#F00",
                    pointRadius: 0,
                    data: [
                        { x: "2020-07-16", y: 0.00 },
                        { x: "2020-07-19", y: 60.59 },
                        { x: "2020-08-07", y: 128.59 },
                        { x: "2020-09-03", y: 345.52 },
                        { x: "2020-09-21", y: 562.84 },
                        { x: "2020-09-30", y: 585.06 },
                        { x: "2020-10-01", y: 591.38 },
                        { x: "2020-10-02", y: 600.72 },
                        { x: "2020-10-03", y: 584.80 }
                    ]
                },
                {
                    label: "AUD",
                    borderColor: "#00F",
                    pointRadius: 0,
                    data: [
                        { x: "2020-07-16", y: 0.00   / 0.68566 },
                        { x: "2020-07-19", y: 60.59  / 0.68566 },
                        { x: "2020-08-07", y: 128.59 / 0.72323 },
                        { x: "2020-09-03", y: 345.52 / 0.733 },
                        { x: "2020-09-21", y: 562.84 / 0.72658 },
                        { x: "2020-09-30", y: 585.06 / 0.71205 },
                        { x: "2020-10-01", y: 591.38 / 0.72020 },
                        { x: "2020-10-02", y: 600.72 / 0.715 },
                        { x: "2020-10-03", y: 584.80 / 0.71635 }
                    ]
                }]
            }
        }


        this.chartReference = React.createRef();
    }

    async fetchState() {
        const response = await fetch('/line.json')
        const json = await response.json()
        this.setState({
            ValueOverTime: json
        })
    }

    componentDidMount() {
        // this.fetchState()
    }

    render() {
        return (
            <div>
                {/* <h2>Value Line</h2> */}
                <NewLine
                    ref={this.chartReference}
                    data={this.state.ValueOverTime}
                    width={48}
                    height={50}
                    options={{
                        animation: {
                            animateScale: false
                        },
                        legend: {
                            position: "top",
                            align: "center"
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                type: "time",
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: "Date"
                                },
                                time: {
                                    unit: "day",
                                    stepSize: 7
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: "Value"
                                }
                            }]
                        }

                    }}
                    redraw
                />
            </div>
        );
    }
}

export default ValueLine;