import React from 'react';
import { Doughnut, Chart, defaults } from 'react-chartjs-2';
import pattern from 'patternomaly';

function withChartSizeControl(Component) {
	return props => (
		<div
			className="chart"
			style={{
				position: "relative"
				, height: props.height+"%"
				, width: props.width+"%"
			}}
		>
			<Component {...props} />
		</div>
	);
}

const NewDoughnut = withChartSizeControl(Doughnut);

class EquityPie extends React.Component {
    constructor(props) {
        defaults.global.animation = false;
        super(props)
        this.state = {
            equities: {
                // labels: ["loading"],
                datasets: [{
                    data: [0.00001]
                }]
            }
        }

        this.chartReference = React.createRef();
    }

    async fetchState() {
        const response = await fetch('http://localhost:8080/graphData/equityPie')
        let json = await response.json()

        json.datasets[0].backgroundColor
            = pattern.generate(json.datasets[0].backgroundColor)

        this.setState({
            equities: json
        })
    }

    componentDidMount() {
        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    // Get ctx from string
                    let ctx = chart.chart.ctx;

                    // Get options from the center object in options
                    let centerConfig = chart.config.options.elements.center;
                    let fontStyle = centerConfig.fontStyle || 'Arial';
                    let txt = centerConfig.text;
                    let color = centerConfig.color || '#000';
                    let maxFontSize = centerConfig.maxFontSize || 75;
                    let sidePadding = centerConfig.sidePadding || 20;
                    let sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    // Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    let stringWidth = ctx.measureText(txt).width;
                    let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    let widthRatio = elementWidth / stringWidth;
                    let newFontSize = Math.floor(30 * widthRatio);
                    let elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    let fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
                    let minFontSize = centerConfig.minFontSize;
                    let lineHeight = centerConfig.lineHeight || 25;
                    let wrapText = false;

                    if (minFontSize === undefined) {
                        minFontSize = 20;
                    }

                    if (minFontSize && fontSizeToUse < minFontSize) {
                        fontSizeToUse = minFontSize;
                        wrapText = true;
                    }
                    if (centerConfig.textWrap) {
                        wrapText = true
                    }

                    // Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    if (!wrapText) {
                        ctx.fillText(txt, centerX, centerY);
                        return;
                    }

                    let words = txt.split(' ');
                    let line = '';
                    let lines = [];

                    // // Break words up into multiple lines if necessary
                    for (let n = 0; n < words.length; n++) {
                        let testLine = line + words[n] + ' ';
                        let metrics = ctx.measureText(testLine);
                        let testWidth = metrics.width;
                        if (words[n] === "<br>" || (testWidth > elementWidth && n > 0)) {
                            lines.push(line);
                            if (words[n] !== "<br>") {
                                line = words[n] + ' ';
                            } else {
                                line = ''
                            }
                        } else {
                            line = testLine;
                        }
                    }

                    // Move the center up depending on line height and number of lines
                    centerY -= (lines.length / 2) * lineHeight;

                    for (let n = 0; n < lines.length; n++) {
                        ctx.fillText(lines[n], centerX, centerY);
                        centerY += lineHeight;
                    }
                    //Draw text in center
                    ctx.fillText(line, centerX, centerY);
                }
            }
        });

        this.fetchState()
    }

    getTotalValue() {
        let total = 0.0
        this.state.equities.datasets[0].data.forEach(num => {
            total = total + parseFloat(num)
        });
        return total
    }

    render() {
        return (
            <div>
                {/* <h2>Equity Pie</h2> */}
                <NewDoughnut
                    // ref={this.chartReference}
                    data={this.state.equities}
                    width={50}
                    height={500}

                    options={{
                        maintainAspectRatio: false,
                        legend: {
                            position: "left",
                            align: "middle",
                        },
                        elements: {
                            center: {
                                text: "$" + this.getTotalValue().toFixed(2) + " <br> (USD) Total",
                                color: '#555', // Default is #000000
                                sidePadding: 20, // Default is 20 (as a percentage)
                                minFontSize: 20, // Default is 20 (in px), set to false and text will not wrap.
                                textWrap: true,
                                lineHeight: 25, // Default is 25 (in px), used for when text wraps
                            }
                        }
                    }}
                    redraw
                />
            </div>
        );
    }
}

export default EquityPie;